<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use App\Traits\S3Trait;

class Job extends Model
{

    use S3Trait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id','user_id','title','description',
        'address','mail','minimal_experience',
        'minimum_knowledge','starting_salary','final_salary',
        'vacancy_number','social_benefit','lat','lon',
        'type_time','type_job','image', 'active', 'featured'
    ];

    public function getCreatedAtAttribute($value) {
        if(isset($value)) {
            $date = new Carbon($value);
            return $date->format('d-m-Y');
        }

        return $value;
    }

    public function getImageAttribute($value) {

        if(config('filesystems.default') == 's3') return $this->getAmazonUrlToken($value);

        if(isset($value)) {
            return asset("storage/$value");
        }

        return $value;
    }

    public function canEdit() {
        $user = auth()->user();
        $user_id = "-1";
        if($user) $user_id = $user->id;
        return $this->user_id == $user_id;
    }

    public function registerToOffert() {
        $user = auth()->user();
        return $this->users()->where('user_id', $user->id)->get()->count();
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
