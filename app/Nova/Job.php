<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Http\Requests\NovaRequest;

use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;


class Job extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Job';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'title', 'description',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [

            ID::make()->sortable(),

            Text::make('Title', 'title')
                ->sortable()
                ->rules('required', 'max:255'),

            /*Textarea::make('Description', 'description')->alwaysShow(),
            Text::make('Address', 'address')->hideFromIndex(),
            Text::make('Email', 'mail')->hideFromIndex(),*/
            // minimal_experience
            /*Text::make('Minimum Knowledge', 'minimum_knowledge')->hideFromIndex(),
            Text::make('Starting Salary', 'starting_salary')->hideFromIndex(),
            Text::make('Final Salary', 'final_salary')->hideFromIndex(),
            Number::make('Vacancy', 'vacancy_number')
                ->min(1)
                ->step(1),
            Text::make('Social Benefit', 'social_benefit')->hideFromIndex(),*/
            // type_time
            // type_job
            Boolean::make('Active', 'active'),
            Boolean::make('Featured', 'featured'),

            BelongsTo::make('Company', 'user', User::class),
            BelongsToMany::make('Users', 'users', User::class),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
