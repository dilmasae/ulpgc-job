<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

use App\Traits\S3Trait;

use App\Notifications\CustomResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable, HasRoles, S3Trait;

    protected $guard_name = 'web';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email', 'birth_date', 'phone', 'address', 'occupation', 'cv', 'gender', 'password', 'company_name', 'image', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Return user rol
     *
     * @return String
     */
    public function getRol() {
        return $this->getRoleNames()->first();
    }

    public function getImageAttribute($value) {

        if(config('filesystems.default') == 's3') return $this->getAmazonUrlToken($value);

        if(isset($value)) {
            return asset("storage/$value");
        }

        return $value;
    }

    public function getCvAttribute($value) {

        if(config('filesystems.default') == 's3') return $this->getAmazonUrlToken($value);

        if(isset($value)) {
            return asset("storage/$value");
        }

        return $value;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomResetPasswordNotification($token, $this->email));
    }

    public function offers()
    {
        return $this->hasMany(Job::class);
    }

    public function candidatures()
    {
        return $this->belongsToMany(Job::class);
    }

    public function jobs()
    {
        return $this->hasMany(User::class);
    }
}
