<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function index()
    {

    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        return view('profile.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail(auth()->user()->id);
        $user->fill($request->except(['cv', 'image', 'password']));

        if(isset($request->password)) {
            $user->password = Hash::make($request->password);
        }

        $user->save();
        return redirect()->back()->with(['message' => __('messages.save_correctly')]);
    }

    public function destroy($id)
    {
        $user = auth()->user();
        if($user->id != $id) return redirect()->back();

        $inactiveUser = User::findOrFail($id);
        $inactiveUser->active = false;
        $inactiveUser->save();
        Auth::logout();
        return redirect('/');
    }
}
