<?php

namespace App\Http\Controllers;

use App\Job;
use Illuminate\Http\Request;

class CandidatureController extends Controller
{

    public function addCandidature($job_id)
    {
        $user = auth()->user();

        if(Job::find($job_id)->registerToOffert()) {
            $user->candidatures()->detach($job_id);
        }
        else {
            $user->candidatures()->attach($job_id);
        }

        return redirect()->back()->with(['message' => __('messages.save_correctly')]);
    }

    public function index()
    {
        $user = auth()->user();
        $candidatures = $user->candidatures()->paginate();

        return view('candidatures.job_offer', compact('candidatures'));
    }
}
