<?php

namespace App\Http\Controllers;

use App\Job;
use App\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{

    public function index()
    {
        $jobs = Job::paginate();
        $user = auth()->user();
        return view('profile.index', compact("user", "jobs"));
    }

    public function edit()
    {
        $user = auth()->user();
        return view('profile.edit', compact('user'));
    }

    public function photo(Request $request)
    {
        $id = auth()->user()->id;
        $user = User::findOrFail($id);

        if(isset($request->image)) {
            Storage::delete($user->image);
            $uri = $request->file('image')->store("user/{$user->id}");
            $user->image = $uri;
        }

        $user->save();

        return redirect()->back()->with(['message' => __('messages.save_correctly')]);

    }

    public function deletePhoto() {
        $id = auth()->user()->id;
        $user = User::findOrFail($id);
        Storage::delete($user->image);
        $user->image = null;
        $user->save();

        return redirect()->back()->with(['message' => __('messages.save_correctly')]);
    }

    public function addCv(Request $request) {

        $user = User::findOrFail(auth()->user()->id);

        if(isset($request->cv)) {
            Storage::delete($user->cv);
            $uri = $request->file('cv')->store("user/{$user->id}");
            $user->cv = $uri;
        }

        $user->save();

        return response()->json([
            'user' => $user,
        ], 201);
    }

    public function removeCv(Request $request) {
        $user = User::findOrFail(auth()->user()->id);
        Storage::delete($user->cv);
        $user->cv = null;
        $user->save();

        return response()->json([
            'user' => $user,
        ], 200);
    }

}
