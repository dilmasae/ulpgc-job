<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Job;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{

    public function index()
    {
        $features = Job::where('featured', true)->get();
        return view('home', compact('features'));
    }

    public function setLocale($locale)
    {
        Session::put('locale', $locale);
        return redirect()->back();
    }

}
