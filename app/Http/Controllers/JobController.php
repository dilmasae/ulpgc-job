<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Job;
use App\Category;
use App\Enums\ExperienceEnum;
use App\Enums\JobTimeEnum;
use App\Enums\JobTypeEnum;

class JobController extends Controller
{

    public function index(Request $request)
    {

        $jobs = Job::with(['category']);
        $experiences = ExperienceEnum::All();
        $jobTime = JobTimeEnum::All();
        $jobType = JobTypeEnum::All();

        session()->flashInput($request->input());

        $search = $request->query("search");
        if(isset($search)) {
            $likeSearch = "%${search}%";
            $jobs->where("title", "like", $likeSearch)
                 ->orWhere("description", "like", $likeSearch)
                 ->orWhere("address", "like", $likeSearch);
        }

        $active = $request->query("active");
        if(isset($active)) {
            if($active == "true") $jobs->where("active", true);
            else $jobs->where("active", false);
        }
        else {
            $jobs->where("active", true);
        }

        $minimalExperience = $request->query('minimal_experience');
        if(isset($minimalExperience)) {
            if(!is_array($minimalExperience)) return redirect()->back();
            $jobs->where(function ($query) use($minimalExperience) {
                foreach($minimalExperience as $experience) {
                    $query->orWhere('minimal_experience', $experience);
                }
            });
        }

        $typeTime = $request->query('type_time');
        if(isset($typeTime)) {
            if(!is_array($typeTime)) return redirect()->back();
            $jobs->where(function ($query) use($typeTime) {
                foreach($typeTime as $time) {
                    $query->orWhere('type_time', $time);
                }
            });
        }

        $typeJob = $request->query('type_job');
        if(isset($typeJob)) {
            if(!is_array($typeJob)) return redirect()->back();
            $jobs->where(function ($query) use($typeJob) {
                foreach($typeJob as $type) {
                    $query->orWhere('type_job', $type);
                }
            });
        }

        return view('jobs.index', [
            'experiences'   => $experiences,
            'jobTime'       => $jobTime,
            'jobType'       => $jobType,
            'jobs'          => $jobs->orderBy('id', 'desc')->paginate()->appends(request()->query()),
        ]);
    }

    public function create()
    {
        $job = new Job();
        $experiences = ExperienceEnum::All();
        $jobTime = JobTimeEnum::All();
        $jobType = JobTypeEnum::All();
        $categories = Category::All();

        return view('jobs.edit', compact(['job', 'experiences', 'jobTime', 'jobType', 'categories']));
    }

    public function store(Request $request)
    {

        $job = new Job();
        $job->fill($request->except(['image']));
        $job->user_id = auth()->user()->id;
        $job->save();

        if(isset($request->image)) {
            Storage::delete($job->image);
            $uri = $request->file('image')->store("job/{$job->id}");
            $job->image = $uri;
        }

        $job->save();

        return redirect('offers')->with(['message' => __('messages.save_correctly')]);

    }

    public function show(Job $job)
    {
        return view('jobs.show', compact('job'));
    }

    public function edit(Job $job)
    {
        if(!$this->is_my_job($job)) return redirect()->back();

        $experiences = ExperienceEnum::All();
        $jobTime = JobTimeEnum::All();
        $jobType = JobTypeEnum::All();
        $categories = Category::All();

        return view('jobs.edit', compact(['job', 'experiences', 'jobTime', 'jobType','categories']));
    }

    public function update(Request $request, Job $job)
    {

        if(!$this->is_my_job($job)) return redirect()->back();

        $job->fill($request->except(['image']));

        if(isset($request->image)) {
            Storage::delete($job->image);
            $uri = $request->file('image')->store("job/{$job->id}");
            $job->image = $uri;
        }

        $job->save();

        return redirect()->back()->with(['message' => __('messages.save_correctly')]);
    }

    public function destroy(Job $job)
    {
        $job->active = false;
        $job->save();
        return redirect()->back()->with(['message' => __('messages.save_correctly')]);
    }

    public function getOffers()
    {
        $user = auth()->user();
        $jobs = $user->offers()->orderBy('id', 'desc')->paginate();

        return view('offers.index', compact('jobs'));
    }
}
