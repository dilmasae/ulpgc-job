<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Job;
use App\Enums\RolEnum;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function is_my_job(Job $job) {
        $me = auth()->user();

        switch($me->getRol()) {
            case RolEnum::Admin:
                return true;
            default:
                return $job->canEdit();
                break;
        }

        return false;
    }

}
