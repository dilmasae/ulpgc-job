<?php

namespace App\Enums;

abstract class RolEnum {

    const User = "user";
    const Company = "company";
    const Admin = "admin";

    static public function All() : array {
        return [
            self::User,
            self::Company,
        ];
    }

    static public function translate(?string $rol) : string {
        if(is_null($rol)) return '';
        switch($rol) {
            case self::User:
                return __('messages.enum.rol.user');
            case self::Company:
                return __('messages.enum.rol.company');
            default:
                return '';
        }
    }

}
