<?php

namespace App\Enums;

abstract class ExperienceEnum {

    const Junior = "junior";
    const Senior = "senior";

    static public function All() : array {
        return [
            self::Junior,
            self::Senior,
        ];
    }

    static public function translate(string $experience) : string {
        switch($experience) {
            case self::Junior:
                return __('messages.enum.experience.junior');
            case self::Senior:
                return __('messages.enum.experience.senior');
            default:
                return '';
        }
    }

}
