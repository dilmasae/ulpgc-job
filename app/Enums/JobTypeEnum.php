<?php

namespace App\Enums;

abstract class JobTypeEnum {

    const Job = "job";
    const BusinessPractice = "business_practice";
    const WorkGrant = "work_grant";

    static public function All() : array {
        return [
            self::Job,
            self::BusinessPractice,
            self::WorkGrant,
        ];
    }

    static public function translate(?string $type) : string {
        if(is_null($type)) return '';
        switch($type) {
            case self::Job:
                return __('messages.enum.job_type.job');
            case self::BusinessPractice:
                return __('messages.enum.job_type.business_practice');
            case self::WorkGrant:
                return __('messages.enum.job_type.work_grant');
            default:
                return '';
        }
    }

}
