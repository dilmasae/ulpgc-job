<?php

namespace App\Enums;

abstract class JobTimeEnum {

    const FullTime = "full_time";
    const PartTime = "part_time";
    const Temporary = "temporary";

    static public function All() : array {
        return [
            self::FullTime,
            self::PartTime,
            self::Temporary,
        ];
    }

    static public function translate(?string $time) : string {
        if(is_null($time)) return '';
        switch($time) {
            case self::FullTime:
                return __('messages.enum.job_time.full_time');
            case self::PartTime:
                return __('messages.enum.job_time.part_time');
            case self::Temporary:
                return __('messages.enum.job_time.temporary');
            default:
                return '';
        }
    }

}
