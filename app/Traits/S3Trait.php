<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;

trait S3Trait {

    function getAmazonUrlToken($uri) {
        if($uri == null) return null;
        $s3 = Storage::disk('s3');
        $client = $s3->getDriver()->getAdapter()->getClient();
        $expiry = "+30 minutes";

        $command = $client->getCommand('GetObject', [
            'Bucket' => config('filesystems.disks.s3.bucket'),
            'Key'    => $uri
        ]);

        $request = $client->createPresignedRequest($command, $expiry);

        return (string) $request->getUri();
    }

}
