<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Enums\ExperienceEnum;
use App\Enums\JobTimeEnum;
use App\Enums\JobTypeEnum;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('category_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('address')->nullable();
            $table->string('mail');
            $table->enum('minimal_experience', ExperienceEnum::All())->nullable();
            $table->text('minimum_knowledge')->nullable();
            $table->double('starting_salary')->nullable();
            $table->double('final_salary')->nullable();
            $table->integer('vacancy_number')->default(1);
            $table->text('social_benefit')->nullable();
            $table->enum('type_time', JobTimeEnum::All())->nullable();
            $table->enum('type_job', JobTypeEnum::All())->nullable();
            $table->string('image')->nullable();
            $table->double('lat', 10, 7)->nullable();
            $table->double('lon', 10, 7)->nullable();
            $table->boolean('active')->default(true);
            $table->boolean('featured')->default(false);
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
