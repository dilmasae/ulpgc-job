<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});

$factory->defineAs(App\User::class, 'admin', function ($faker) {
    $user = $this->raw(App\User::class);
    return array_merge($user, [
        'first_name' => 'Admin',
        'last_name' => 'Admin',
        'email' => 'admin@tfg.com'
    ]);
});

$factory->defineAs(App\User::class, 'company', function ($faker) {
    $user = $this->raw(App\User::class);
    return array_merge($user, [
        'first_name' => 'Company',
        'last_name' => 'Company',
        'email' => 'company@tfg.com'
    ]);
});

$factory->defineAs(App\User::class, 'user', function ($faker) {
    $user = $this->raw(App\User::class);
    return array_merge($user, [
        'first_name' => 'User',
        'last_name' => 'User',
        'email' => 'user@tfg.com'
    ]);
});

