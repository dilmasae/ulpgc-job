<?php

use App\Job;
use Illuminate\Database\Seeder;

use App\Enums\ExperienceEnum;
use App\Enums\JobTimeEnum;
use App\Enums\JobTypeEnum;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create("es_ES");

        $experiences = ExperienceEnum::All();
        $times = JobTimeEnum::All();
        $types = JobTypeEnum::All();

        for($i = 0; $i < 100; $i++){
            Job::create([
                'user_id' => 2,
                'title'=> $faker->jobTitle,
                'description'=> $faker->text,
                'mail'=> $faker->email,
                'address'=> $faker->address,
                'minimal_experience' => $experiences[rand(0, count($experiences) - 1)],
                'type_time' => $times[rand(0, count($times) - 1)],
                'type_job' => $types[rand(0, count($types) - 1)],
            ]);
        }
    }
}
