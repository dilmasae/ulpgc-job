<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class PermissionsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $adminRole = Role::create(['name' => 'admin']);
        $companyRole = Role::create(['name' => 'company']);
        $userRole = Role::create(['name' => 'user']);

        $users = User::all();
        $i = 0;

        foreach($users as $user) {

            if($user->id == 1) $user->assignRole('admin');
            if($user->id == 2) $user->assignRole('company');
            else $user->assignRole('user');

            $i++;
        }

    }
}
