<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 'admin')->create();
        factory(App\User::class, 'company')->create();
        factory(App\User::class, 'user')->create();
        factory(App\User::class, 100)->create()->each(function ($user) {
            //$user->posts()->save(factory(App\Post::class)->make());
        });

        $this->call(JobsTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
    }
}
