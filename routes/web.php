<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('locale/{locale}', 'HomeController@setLocale');

Route::post('/login/active', 'Auth\LoginController@authenticate');
Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/', 'HomeController@index');

Route::resource('/users', 'UserController');
Route::resource('/jobs', 'JobController');
Route::get('/profile', 'ProfileController@index')->middleware('auth');
Route::post('/profile/photo', 'ProfileController@photo')->middleware('auth');
Route::get('/profile/photo/delete', 'ProfileController@deletePhoto')->middleware('auth');
Route::post('/add/cv','ProfileController@addCv');
Route::get('/remove/cv','ProfileController@removeCv');

Route::get('/jobs/candidature/{id}', 'CandidatureController@addCandidature')->middleware('role:user');
Route::get('/candidatures','CandidatureController@index')->middleware('role:user');

Route::get('/offers','JobController@getOffers')->middleware('role:company');
