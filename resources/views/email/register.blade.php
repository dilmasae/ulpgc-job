@extends('templates.email')

@section('body')

    <div class="mt-3">
        <img class="img-center logo" src="{{asset('images/email_logo.jpg')}}" alt="logo">
    </div>

    <div class="container">

        <div class="shadow">

            <header class="mt-3 header separator">{{ __('messages.wellcome_to') }} {{env('APP_NAME')}}</header>
            <main class="main separator">

                <p>{{ __('messages.hello') }} {{$user->first_name ?? ""}} {{$user->last_name ?? ""}}.</p>
                <p>{{ __('messages.email_register_text') }}</p>

            </main>

        </div>

    </div>

@endsection
