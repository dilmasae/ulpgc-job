@extends('templates.front_with_search')

@section('body')
    <div class="container">
        <div class="py-5">

            @include('components.show_messages')

            <div class="d-flex">
                <h3 class="h3 p-b-20px">{{ __('messages.my_offers') }}</h3>
                <a class="ml-auto" href="{{ url('jobs/create') }}">
                    <button class="btn btn-primary btn-sm color-white">{{ __('messages.add') }}</button>
                </a>
            </div>
            @isset($jobs)
            @if(count($jobs))
                <div class=" table-responsive">
                    <table class="profile__table w-100">
                        <tr class="table-title">
                            <th>{{ __('messages.name') }}</th>
                            <th>{{ __('messages.location') }}</th>
                            <th>{{ __('messages.state') }}</th>
                            <th>{{ __('messages.publication_date') }}</th>
                            <th></th>
                        </tr>
                        @foreach($jobs as $job)
                            <tr>
                                <td>{{ $job->title }}</td>
                                <td>{{ $job->address }}</td>
                                <td>
                                    @include('components.job_state', ['job' => $job])
                                </td>
                                <td>{{ $job->created_at }}</td>
                                <td>
                                    <a class="btn-primary btn btn-sm color-white" href="{{ url('jobs') }}/{{ $job->id }}">{{__('messages.see')}}</a>
                                    @if($job->active)
                                        <a class="btn-danger btn btn-sm js-btn-confirm" data-type="job" data-identifier="{{ $job->id }}" href="#">{{__('messages.cancel')}}</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            @else
                <div>
                    {{ __('messages.offers_no_content') }}
                </div>
            @endif
            <div class="mt-3">
                {{ $jobs->links() }}
            </div>
            @endisset
            {{-- <div class="row">
                <div class="col-12 offset-md-2 col-md-8 ">
                    @foreach($jobs as $job)
                        @component('jobs.job_item',
                                    ['job' => $job,
                                     'more' => 'more'
                                     ])
                        @endcomponent
                    @endforeach
                    {{ $jobs->links() }}
                </div>
            </div> --}}

        </div>

        <!-- Modal -->
        <div class="modal fade bd-example-modal-sm" id="btn-confirm-wrapper" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <form class="modal-content" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('messages.my_offers_delete_confirmation') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm " data-dismiss="modal">{{ __('messages.close') }}</button>
                        <button type="submit" class="btn-danger btn btn-sm">{{ __('messages.cancel') }}</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection
