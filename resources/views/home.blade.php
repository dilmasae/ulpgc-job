@extends('templates.front')

@section('body')
    <div class="home">
        <form action="{{ url('/jobs') }}" class="home__search position-relative" style="background-image: url(' {{ asset('images/asset/foto1.jpeg') }} ')">
            <div class="container home__search__form">
                <div class="input-group">
                    <input type="text" name="search" class="form-control" placeholder="{{ __('messages.search') }}" required>
                    <div class="input-group-append">
                        <button class="btn btn-secondary" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </div>
            <!--<div class="home__arrow js-home__arrow">
                <span><i class="fas fa-chevron-down"></i></span>
            </div> -->
        </form>
        <div style="background-color: white">
            <div class="container">
                <div class="p-y-30px">
                    <div class="row">
                        <div class="col-6 col-lg-3">
                            <div class="box">
                                <a href="https://www.ulpgc.es/">
                                    <div class="box__item">
                                        <img class="img-fluid img" src="{{ asset('images/logos-home/college.svg') }}" alt="{{__('messages.college')}}">
                                        <p class="box-title">{{__('messages.college')}}</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-6 col-lg-3">
                            <div class="box">
                                @php $typeUrl = \App\Enums\JobTypeEnum::Job; @endphp
                                <a href="{{ url("/jobs?search=&type_job%5B%5D=$typeUrl") }}">
                                    <div class="box__item">
                                        <img class="img" src="{{ asset('images/logos-home/bag.svg') }}" alt="{{__('messages.job')}}">
                                        <p class="box-title">{{__('messages.job')}}</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-6 col-lg-3">
                            <div class="box">
                                @php $typeUrl = \App\Enums\JobTypeEnum::BusinessPractice; @endphp
                                <a href="{{ url("/jobs?search=&type_job%5B%5D=$typeUrl") }}">
                                    <div class="box__item">
                                        <img class="img" src="{{ asset('images/logos-home/graduation.svg') }}" alt="{{__('messages.business_practices')}}">
                                        <p class="box-title">{{__('messages.business_practices')}}</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-6 col-lg-3">
                            <div class="box">
                                @php $typeUrl = \App\Enums\JobTypeEnum::WorkGrant; @endphp
                                <a href="{{ url("/jobs?search=&type_job%5B%5D=$typeUrl") }}">
                                    <div class="box__item">
                                        <img class="img" src="{{ asset('images/logos-home/global.svg') }}" alt="{{ __('messages.scholarships') }}">
                                        <p class="box-title">{{ __('messages.scholarships') }}</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="home__item">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 d-flex flex-column justify-content-center">
                        <h2 class="home__item__title">{{ __('messages.home_section_title') }}</h2>
                        <p class="home__item__description">
                            {{ __('messages.home_section') }}
                        </p>
                    </div>
                    <div class="col12 col-md-6">
                        <div class="home__item__img" style="background-image: url({{ asset('images/foto2.png') }})">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($features->count())
    <div class="wrapped">
        <div class="container">
            <h2 class="pb-5">{{ __('messages.offers_title_home') }}</h2>
            <div class="swiper-container">
                <div class="swiper-wrapper">

                    @foreach ($features as $feature)
                        <div class="swiper-slide">
                            <div class="card">
                                @if($feature->image)
                                    <img class="card-img-top image-card" src=" {{ $feature->image }} " alt="Card image cap">
                                @else
                                    <img class="card-img-top " src="{{ asset('images/no-image.jpg') }}" alt="no-image">
                                @endif
                                <div class="card-body">
                                    <h5 class="card-title">{{ $feature->title }}</h5>
                                    <div class="wrapped__link">
                                        <a href="#" class="link p-r-9px">{{ __('messages.see_more') }}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
    </div>
    @endif
@endsection

