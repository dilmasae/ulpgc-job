@extends('templates.front_with_search')

@section('body')

    <div class="container">
        <div class="py-5">

            @include('components.show_messages')

            <h3 class="h3 p-b-20px">{{ __('messages.my_candidatures') }}</h3>
            @isset($candidatures)
                @if(count($candidatures))
                    <div class=" table-responsive">
                        <table class="profile__table w-100">
                            <tr class="table-title">
                                <th>{{ __('messages.name') }}</th>
                                <th>{{ __('messages.location') }}</th>
                                <th>{{ __('messages.state') }}</th>
                                <th>{{ __('messages.publication_date') }}</th>
                                <th></th>
                            </tr>
                            @foreach($candidatures as $candidature)
                                <tr>
                                    <td>{{ $candidature->title }}</td>
                                    <td>{{ $candidature->address }}</td>
                                    <td>
                                        @include('components.job_state', ['job' => $candidature])
                                    </td>
                                    <td>{{ $candidature->created_at }}</td>
                                    <td>
                                        <a class="btn btn-primary btn-sm color-white" href="{{ url('jobs', $candidature->id) }}">{{__('messages.see')}}</a>
                                        <a class="btn-danger btn btn-sm js-btn-confirm" data-type="candidature" data-identifier="{{ $candidature->id }}" href="#">{{__('messages.unlink')}}</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>

                    </div>
                    <div class="profile__pagination">
                        {{ $candidatures->links() }}
                    </div>
                @else
                    <div>
                        {{ __('messages.candidatures_no_content') }}
                    </div>
                @endif
            @endisset
        </div>

        <!-- Modal -->
        <div class="modal fade bd-example-modal-sm" id="btn-confirm-wrapper" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <form class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('messages.my_candidatures_delete_confirmation') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm " data-dismiss="modal">{{ __('messages.close') }}</button>
                        <button type="submit" class="btn-danger btn btn-sm">{{ __('messages.unlink') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection



