<form method="get" action="{{ url('jobs') }}">
    <div class="search">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 d-none d-lg-inline-block">
                    <div class="search__logo">
                        <img  class="image" src="{{asset('/images/logos/ulpgc-logo.jpg')}}" alt="{{__('messages.ulpgc') }}">
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div>
                        <div class="search__form">
                            <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="{{ __('messages.search') }}" value="{{ old('search') }}">
                                <div class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                            {{-- <div><a class="d-block text-right text-white py-2" data-toggle="modal" href="#exampleModalLong">{{ __('messages.advanced_filter') }}</a></div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- End Modal -->
</form>
