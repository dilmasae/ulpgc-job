<header class="header fixed-top">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand link-header-phone link-header d-lg-none" href="{{ url('/') }}"><img class="img-fluid" src="{{ asset('images/logos/logo-ulpgc-blanco.gif') }}" alt="{{ __('messages.ulpgc') }}"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item  ">
                        <a class="link-header text-white {{ Request::is('/') ? 'active' : '' }}" href="{{ url('/') }}">{{ __('messages.home') }}<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="link-header text-white {{ Request::is('jobs') ? 'active' : '' }}" href="{{ url('/jobs') }}">{{ __('messages.job') }}</a>
                    </li>
                    @role('user')
                    <li class="nav-item">
                        <a class="link-header text-white {{ Request::is('candidatures') ? 'active' : '' }}" href="{{ url('/candidatures') }}">{{ __('messages.candidatures') }}</a>
                    </li>
                    @endrole
                    @role('company')
                    <li class="nav-item">
                        <a class="link-header text-white {{ Request::is('offers') ? 'active' : '' }}" href="{{ url('/offers') }}">{{ __('messages.offers') }}</a>
                    </li>
                    @endrole

                </ul>

                <div class="ml-auto d-flex align-items-center">

                    <div class="d-flex p-r-10px">
                        @if( app()->getLocale() == "en" )
                            <a class="text-white" href="{{ url('locale/es') }}">
                                <span class="badge badge-primary">ES</span>
                            </a>
                        @else
                            <a class="text-white" href="{{ url('locale/en') }}">
                                <span class="badge badge-primary">EN</span>
                            </a>
                        @endif
                    </div>

                    @if(Auth::check())
                    <a class="js-header__profile position-relative d-block d-flex align-items-center" href="#">
                        <div class="header__profile">
                            @php $user = auth()->user(); @endphp
                            @if($user->image)
                                <img class="header--img" src="{{ $user->image }}" alt="image">
                            @else
                                <img class="header--img" src="{{ asset('images/no-image.jpg') }}" alt="no-image">
                            @endif
                        </div>
                        <span class="pl-2 text-white">{{ auth()->user()->first_name }}</span>
                        <i class="pl-2 fas fa-chevron-down text-white"></i>

                        <div class="invisible header__profile__content position-absolute">

                            <a href="{{ url("profile") }}" class="dropdown-profile">{{ __('messages.profile') }}</a>
                            @role('admin')
                            <a href="{{ url(config('nova.path')) }}" class="dropdown-profile">{{ __('messages.dashboard') }}</a>
                            @endrole
                            <a class="dropdown-profile" href="{{ route('logout') }}">{{ __('Logout') }}</a>
                        </div>

                    </a>

                <div class="d-flex align-items-center">

                    @else
                        <li class="p-r-10px">
                            <a class="btn btn-link-header" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        <li class="p-r-10px">
                            <a class="btn btn-link-header" href="{{ route('register') }}">{{ __('messages.register') }}</a>
                        </li>
                    @endif
                </div>

                </div>

            </div>

        </nav>
    </div>
</header>
