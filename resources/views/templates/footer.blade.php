<footer>

    <div class="footer__content">
        <div class="container">
            <div class="row p-20px">
                <div class="col-12 col-md-3">
                    <img class="img-fluid img-logo" src=" {{ asset('images/logo-uplgc-job.png') }}" alt="">
                </div>

                <div class="col-12 col-md-4">
                    <p class="footer-title without-hover m-0 p-b-15px">{{ __('messages.contact') }}</p>
                    <ul class="p-0">
                        <li>
                            <span class="d-block p-y-5px">Juan de Quesada, 30 35001 <br>Las Palmas de Gran Canaria España</span>
                        </li>
                        <li>
                            <a class="footer__item icon" href="mailto:sie@ulpgc.es">
                                <span class="p-r-7px"><i class="fas fa-envelope"></i></span>ulpgcjob@ulpgc.es</a>
                        </li>
                        <li>
                            <a class="footer__item icon" href="#">
                                <span class="p-r-7px"><i class="fas fa-phone-alt"></i></span>+34 928 451 075</a>
                        </li>
                        <li>
                            <a class="footer__item icon" href="#">
                                <span class="p-r-7px"><i class="fab fa-whatsapp"></i></span>
                                +34 660 599 038</a>
                        </li>
                        <li>
                            <a class="footer__item icon" target="_blank" href="https://goo.gl/maps/Du1BZEUTcV7586sD8">
                                <span class="p-r-7px"><i class="fas fa-map-marker-alt"></i></span>
                                {{ __('messages.address_ulpgc') }}</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 offset-md-1 col-md-4 footer__media">
                    <p class="footer-title without-hover m-0 p-b-15px">{{ __('messages.follow_us') }}</p>
                    <div>
                        <div class="footer__media__icon">
                            <div class="row">
                                <div class="col-12">
                                    <a target="_blank"  href="https://www.facebook.com/ULPGC/"><img class="social-media" src="{{ asset('images/icons/001-facebook.svg') }}" alt="Facebook"></a>
                                    <a target="_blank" href="https://www.linkedin.com/company/ulpgc/"><img class="social-media" src="{{ asset('images/icons/010-linkedin.svg') }}" alt="linkedin"></a>
                                    <a target="_blank" href="https://twitter.com/ulpgc"><img class="social-media" src="{{ asset('images/icons/013-twitter-1.svg') }}" alt="Twitter"></a>
                                </div>
                                <div class="col-12">
                                    <a target="_blank" href="https://www.youtube.com/user/ulpgc"><img class="social-media" src="{{ asset('images/icons/008-youtube.svg') }}" alt="Youtube"></a>
                                    <a target="_blank" href="https://www.instagram.com/ulpgc_para_ti/"><img class="social-media" src="{{ asset('images/icons/011-instagram.svg') }}" alt="Instagram"></a>
                                    <a target="_blank" href="https://www.flickr.com/photos/ulpgc"><img class="social-media" src="{{ asset('images/icons/flickr.svg') }}" alt="Flickr"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-separator"></div>
            <span class="footer-subtitle">Copyright © - ULPGC</span>
        </div>
    </div>

</footer>
