<form method="get" action="{{ url('jobs') }}">

    <div class="mb-4">
        <div class="input-group">
        <input type="text" class="form-control" name="search" placeholder="{{ __('messages.search') }}" value="{{ old('search') }}">
            <div class="input-group-append">
                <button class="btn btn-secondary" style="background-color: #ff9a00;border-color: #ff9a00;" type="submit">
                    <i class="fa fa-search"></i>
                </button>
            </div>
        </div>
    </div>

    @isset($experiences)
    <div class="mb-3">
        <h4>{{ __('messages.minimal_experience') }}</h4>
        @foreach($experiences as $experience)
            <div class="form-check p-0">
                <input class="form-check-input mt-0" type="checkbox" name="minimal_experience[]" value="{{ $experience }}" id="cbox_experience_{{$experience}}" @if(in_array($experience, old('minimal_experience', []))) checked @endif>
                <label class="form-check-label pl-2" for="cbox_experience_{{$experience}}">{{ \App\Enums\ExperienceEnum::translate($experience) }}</label>
            </div>
        @endforeach
    </div>
    @endisset

    @isset($jobTime)
    <div class="mb-3">
        <h4>{{ __('messages.type_time') }}</h4>
        @foreach($jobTime as $time)
            <div class="form-check p-0">
                <input class="form-check-input mt-0" type="checkbox" name="type_time[]" value="{{ $time }}" id="cbox_type_time_{{$time}}" @if(in_array($time, old('type_time', []))) checked @endif>
                <label class="form-check-label pl-2" for="cbox_type_time_{{$time}}">{{ \App\Enums\JobTimeEnum::translate($time) }}</label>
            </div>
        @endforeach
    </div>
    @endisset

    @isset($jobType)
    <div class="mb-3">
        <h4>{{ __('messages.type_job') }}</h4>
        @foreach($jobType as $type)
            <div class="form-check p-0">
                <input class="form-check-input mt-0" type="checkbox" name="type_job[]" value="{{ $type }}" id="cbox_type_job_{{$type}}" @if(in_array($type, old('type_job', []))) checked @endif>
                <label class="form-check-label pl-2" for="cbox_type_job_{{$type}}">{{ \App\Enums\JobTypeEnum::translate($type) }}</label>
            </div>
        @endforeach
    </div>
    @endisset

</form>
