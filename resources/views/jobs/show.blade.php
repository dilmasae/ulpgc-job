@extends('templates.front')

@section('body')
    <div class="offer">
        <div class="container">
            <div class="row">
                <div class="col-12 offset-lg-2 col-lg-8">
                    <div class="logo">
                        <div class="row">
                            <div class="col-12 col-sm-10 col-lg-8 image">
                                @if($job->image)
                                    <img class="company_img" src="{{ $job->image }}" alt="image">
                                @else
                                    <img class="company_img" src="{{ asset('images/no-image.jpg') }}" alt="no-image">
                                @endif
                                @if($job->canEdit())
                                <a class="profile__edit text-danger ml-3" href="{{ url('/jobs', [$job->id]) }}/edit">({{ __('messages.edit_offer') }} <i class="fas fa-pencil-alt"></i> )</a>
                                @endif
                            </div>
                            @role('user')
                            @if($job->active)
                            <div class="col-12 col-sm-2 col-lg-4  py-4 py-sm-0">
                                @if($job->registerToOffert())
                                    <a href="{{ url('/jobs/candidature', $job->id) }}" class="btn btn-danger btn-sm ">{{ __('messages.unregister_offer')}}</a>
                                @else
                                    <a href="{{ url('/jobs/candidature', $job->id) }}" class="btn btn-success btn-sm ">{{ __('messages.register_offer')}}</a>
                                @endif
                            </div>
                            @endif
                            @endrole
                        </div>
                    </div>
                    <div class="offer__detail">

                        <div class="mt-3">
                            @include('components.job_state', ['job' => $job])

                            @if($job->canEdit())
                            @php
                                $candidateCount = $job->users()->count();
                                $candidateText = __('messages.candidates');
                                if($candidateCount == 1) {
                                    $candidateText = __('messages.candidate');
                                }
                            @endphp
                            <div id="app">
                                <show-candidates
                                    text="{{ $candidateCount }} {{ $candidateText }}"
                                    noncandidate="{{ __('messages.non_candidate') }}"
                                    nocvtext="{{ __('messages.no_cv') }}"
                                    headertext="{{ __('messages.candidates') }}"
                                    users="{{ $job->users }}"
                                />
                            </div>
                            @endif

                        </div>

                        <h2 class="offer__title">{{ $job->title }}</h2>
                        <div class="button">
                            <img class="icon" src="{{ asset('images/icons/briefcase.svg') }}" alt="Icon">
                            {{ \App\Enums\JobTimeEnum::translate($job->type_time) }}
                        </div>

                        <div class="button">

                            <img class="icon" src="{{ asset('images/icons/money.svg') }}" alt="Icon">
                            @isset($job->starting_salary)
                                <span class="text">{{ $job->starting_salary }}€ - </span>
                            @endisset
                            @isset($job->final_salary)
                                <span class="text">{{ $job->final_salary }}€</span>
                            @endisset

                        </div>
                        <p class="mb-2">{{  __('messages.type_job') }}: {{ \App\Enums\JobTypeEnum::translate($job->type_job) }} </p>

                        <p class="mb-2 ">{{ __('messages.publication_date') }}: {{ $job->created_at }}</p>

                        <div class="py-2 py-lg-4">
                            <h4 class="h4">{{ __('messages.minimum_requirements') }}</h4>

                            @isset($job->minimal_experience)
                            <h5 class="h5">{{ __('messages.minimal_experience') }}</h5>
                            <p>{{ \App\Enums\ExperienceEnum::translate($job->minimal_experience) }}</p>
                            @endisset

                            @isset($job->minimum_knowledge)
                            <h5 class="h5">{{ __('messages.minimum_knowledge') }}</h5>
                            <p> {{ $job->minimum_knowledge }}</p>
                            @endisset

                            @isset($job->description)
                            <h5 class="h5">{{ __('messages.description') }}</h5>
                            <p> {{ $job->description }}</p>
                            @endisset

                            @isset($job->category)
                            <h5 class="h5">{{ __('messages.category') }}</h5>
                            <p> {{ optional($job->category)->name }}</p>
                            @endisset

                            @isset($job->vacancy_number)
                            <h5 class="h5">{{ __('messages.vacancy_number') }}</h5>
                            <p> {{ $job->vacancy_number }}</p>
                            @endisset

                            @isset($job->social_benefit)
                            <h5 class="h5">{{ __('messages.social_benefit') }}</h5>
                            <p> {{ $job->social_benefit }}</p>
                            @endisset

                            @isset($job->lat)
                            <h5 class="h5">{{ __('messages.location') }}</h5>
                            <div id="map" style="width: 99%; height: 400px;"></div>
                            @endisset

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script>

    let map = null;
    let marker = null;

    function initMap() {
        let job = @json($job);
        let mapEl = document.getElementById('map');

        if(!mapEl) return;

        let defaultPosition = {
            lat: 27.9595343,
            lng: -15.728281
        };

        let position = {
            lat: job.lat || null,
            lng: job.lon || null
        }

        if(position.lat == null || position.lng == null)  position = defaultPosition;

        map = new google.maps.Map(mapEl, {
            center: position,
            zoom: 10,
        });

        marker = new google.maps.Marker({
            position: position,
            map: map,
        });

    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_API_KEY')}}&callback=initMap"></script>
@endsection

