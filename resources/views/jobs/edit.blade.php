@extends('templates.front')

@section('body')
    <div class="offer">

        <form method="post" action="{{ url('jobs', $job->id) }}" enctype="multipart/form-data">
            @csrf
            @isset($job->id)
                @method('PUT')
            @endisset
            <div class="container">
                <div class="row">
                    <div class="col-12 offset-lg-2 col-lg-8">

                        @include('components.show_messages')

                        <div class="logo">
                            <div class="row">
                                <div class="col-12 col-sm-6 image">
                                    @if($job->image)
                                        <img class="img-fluid" src="{{ $job->image }}" alt="image">
                                    @else
                                        <img class="img-fluid" src="{{ asset('images/no-image.jpg') }}" alt="no-image">
                                    @endif
                                    <div class="form-group  pt-3">
                                        <label for="job-image"> {{ __('messages.image') }}</label>
                                        <input type="file" name="image" class="form-control-file" id="job-image">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="offer__detail border-right-0">

                            <div class="form-group pt-2">
                                <label for="job-title">{{ __('messages.title') }} <span class="text-danger">*</span></label>
                                <input type="text" name="title" class="form-control" id="job-title" value="{{ $job->title }}" required>
                            </div>

                            <div class="form-group pt-2">
                                <label for="job-email">{{ __('messages.email_contact') }} <span class="text-danger">*</span></label>
                                <input type="email" name="mail" class="form-control" id="job-email" value="{{ $job->mail }}" required>
                            </div>

                            <div class="form-row">

                                <div class="form-group col-md-3">
                                    <label for="job-type">{{ __('messages.type_job') }}</label>
                                    <select class="form-control" id="job-type" name="type_job">
                                    @foreach($jobType as $type)
                                        @php $isSelected = $type == $job->type_job ? 'selected' : null; @endphp
                                        <option value="{{$type}}" {{ $isSelected }}>{{ \App\Enums\JobTypeEnum::translate($type) }}</option>
                                    @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="job-type-time">{{ __('messages.type_time') }}</label>
                                    <select class="form-control" id="job-type-time" name="type_time">
                                        @foreach($jobTime as $type)
                                            @php $isSelected = $type == $job->type_time ? 'selected' : null; @endphp
                                            <option value="{{$type}}" {{ $isSelected }}>{{ \App\Enums\JobTimeEnum::translate($type) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="job-starting-salary">{{ __('messages.starting_salary') }}</label>
                                    <input type="number" min="1" class="form-control" name="starting_salary" id="job-starting-salary" value="{{ $job->starting_salary }}">
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="job-final-salary">{{ __('messages.final_salary') }}</label>
                                    <input type="number" min="1" class="form-control" name="final_salary" id="job-final-salary" value="{{ $job->final_salary }}">
                                </div>

                            </div>

                            @isset($job->created_at)
                            <p class="mb-2 ">{{ __('messages.publication_date') }}: {{ $job->created_at }}</p>
                            @endisset

                            <div class="py-2 py-lg-4">
                                <h4 class="h4">{{ __('messages.minimum_requirements') }}</h4>
                                <h5 class="h5">{{ __('messages.minimal_experience') }}</h5>

                                <div class="form-group">
                                    <select class="form-control" id="job-minimal-experience" name="minimal_experience">
                                        @foreach($experiences as $experience)
                                            @php $isSelected = $experience == $job->minimal_experience ? 'selected' : null; @endphp
                                            <option value="{{$experience}}" {{ $isSelected }}>{{ \App\Enums\ExperienceEnum::translate($experience) }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <h5 class="h5">{{ __('messages.minimum_knowledge') }}</h5>
                                <div class="form-group">
                                    <input type="text" name="minimum_knowledge" class="form-control" id="job-minimal-knowledge" value="{{ $job->minimum_knowledge}}">
                                </div>

                                <h5 class="h5">{{ __('messages.description') }}</h5>
                                <div class="form-group">
                                    <textarea class="form-control" name="description" id="job-description" rows="15">{{ $job->description }}</textarea>
                                </div>

                                <h5 class="h5">{{ __('messages.category') }}</h5>
                                <div class="form-row">
                                    <div class="form-group col-md-5">
                                        <select class="form-control" id="job-category" name="category_id">
                                            <option value="">-</option>
                                            @foreach ($categories as $category)
                                                @php $isSelected = $category->id == $job->category_id ? 'selected' : null; @endphp
                                                <option value="{{$category->id}}" {{ $isSelected }}>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <h5 class="h5">{{ __('messages.vacancy_number') }} <span class="text-danger">*</span></h5>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <input type="number" min="1" name="vacancy_number" class="form-control" id="job-vacancy-number" value="{{ $job->vacancy_number ?? 1}}" reqired>
                                    </div>
                                </div>

                                <h5 class="h5">{{ __('messages.social_benefit') }}</h5>
                                <div class="form-group">
                                    <input type="text" name="social_benefit" class="form-control" id="job-social-benefit" value="{{ $job->social_benefit}}">
                                </div>

                                <h5 class="h5">{{ __('messages.location') }}</h5>
                                <input id="js_input_address" type="hidden" name="address" value="{{ $job->address }}">
                                <p id="js_text_address">{{ $job->address }}</p>
                                <div id="map" style="width: 99%; height: 400px;"></div>
                                <input id="f_lat" type="hidden" name="lat" value="{{ $job->lat }}">
                                <input id="f_lon" type="hidden" name="lon" value="{{ $job->lon }}">

                            </div>

                        </div>

                        <div class="row">
                            <div class="col-12">
                                <input class="btn btn-primary btn-md link m-r-10px btn-block color-white" type="submit" value="{{ __('messages.save') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>
@endsection

@section('js')
<script>

    let map = null;
    let marker = null;
    let geocoder;

    function addMarker(map, latLng) {

        marker = new google.maps.Marker({
            map: map,
            position: latLng,
        });
        let lat = latLng.lat();
        let lon = latLng.lng();

        document.querySelector("#f_lat").value = lat;
        document.querySelector("#f_lon").value = lon;

        codeLatLng(lat, lon);

    }

    function codeLatLng(lat, lng) {

        var latlng = new google.maps.LatLng(lat, lng);
        geocoder.geocode({'latLng': latlng}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {

                if(results.length) {
                    let addr = results[0].formatted_address;
                    $("#js_input_address").val(addr);
                    $("#js_text_address").text(addr);
                }
                else {
                    $("#js_input_address").val("");
                    $("#js_text_address").text("");
                }

            } else {
                console.warn("Geocoder failed due to: " + status);
            }
        });
    }

    function initMap() {
        let job = @json($job);
        let mapEl = document.getElementById('map');

        geocoder = new google.maps.Geocoder();

        if(!mapEl) return;

        let defaultPosition = {
            lat: 27.9595343,
            lng: -15.728281
        };

        let position = {
            lat: Number(job.lat) || null,
            lng: Number(job.lon) || null
        }

        if(position.lat == null || position.lng == null)  position = defaultPosition;

        map = new google.maps.Map(mapEl, {
            center: position,
            zoom: 10,
        });

        marker = new google.maps.Marker({
            position: position,
            map: map,
        });

        // Change marker position
        google.maps.event.addListener(map, 'click', function(e) {
            marker.setMap(null);
            addMarker(map, e.latLng);
        });

    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_API_KEY')}}&callback=initMap"></script>
@endsection
