<div class="job">
    <div class="row">
        <div class="col-sm-2">
            @if($job->image)
                <img class="img-fluid" src=" {{ $job->image }} " alt="Card image cap">
            @else
                <img class="img-fluid" src="{{ asset('images/no-image.jpg') }}" alt="no-image">
            @endif
        </div>

        <div class="col-sm-10">
            <h2 class="h2">{{ $job->title }}
                @if($job->canEdit())
                <span class="">
                        <a class="profile__edit" href="{{ url('/jobs', [$job->id]) }}/edit">({{ __('messages.edit_offer') }} <i class="fas fa-pencil-alt"></i> )</a>
                    </span>
                @endif
            </h2>
            <h5>{{ $job->user->company_name ?? "" }} - <span class="address">{{ $job->address ?? "" }}</span></h5>

            <p class="job__description elipsis p-y-10px">{{ $job->description }}</p>

            <div class="job__item">
                <span class="job__time">{{ $job->created_at }}</span>
                <a class="btn btn-outline-primary btn-sm link m-r-10px" href="mailto:{{ $job->mail }}">{{  __('messages.email') }}</a>
                <a class="btn btn-outline-primary btn-sm link m-r-10px" href="{{ url('/jobs', [$job->id]) }}">{{ __('messages.more')}}</a>
            </div>
        </div>
    </div>
</div>
