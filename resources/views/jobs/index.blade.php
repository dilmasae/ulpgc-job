@extends('templates.front')

@section('body')
    <div class="container">
        <h2 class="title">{{ __('messages.job') }}</h2>
        <div class="row">
            <div class="col-12 col-lg-9">
                @if(count($jobs))
                @foreach($jobs as $job)
                    @component('jobs.job_item',
                                ['job' => $job,
                                 'more' => 'more'
                                 ])
                    @endcomponent
                @endforeach
                {{ $jobs->links() }}
                @else
                    {{ __('messages.without_results') }}
                @endif
            </div>

            <div class="col-12 col-lg-3 order">
                @include('jobs.filter')
            </div>
        </div>
    </div>
@endsection
