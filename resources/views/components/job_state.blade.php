@isset($job)
@if($job->active)
    <span class="badge badge-pill badge-success">{{ __('messages.open') }}</span>
@else
    <span class="badge badge-pill badge-danger">{{ __('messages.closed') }}</span>
@endif
@endisset
