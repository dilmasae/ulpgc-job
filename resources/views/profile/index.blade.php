@extends('templates.front_with_search')

@section('body')
   <section class="profile">
       <div class="container ">
           @isset($user)
               @component('profile.nav_profile', ["user" => $user, "jobs" => $jobs])@endcomponent
           @endisset
       </div>
   </section>
@endsection
