<div class="row">
    <div id="profile_change_image_form" class="col-12 col-lg-4 profile__detail flex-column">
        <div>
        @if($user->image)
            <img class="profile-img" src="{{ $user->image }}" alt="image">
        @else
            <img class="profile-img" src="{{ asset('images/no-image.jpg') }}" alt="no-image">
        @endif

        @if($user->image)
        <a class="mt-3" href="{{ url('/profile/photo/delete') }}">
            <button class="btn m-0-auto d-block">{{ __('messages.delete') }}</button>
        </a>
        <form method="post" action="{{ url('/profile/photo') }}" enctype="multipart/form-data">
            @csrf
            <input id="profile_change_image" class="d-none" type="file" name="image">
            <button class="btn m-0-auto d-block" type="submit">{{ __('messages.change') }}</button>
        </form>
        @else
        <form class="mt-3" method="post" action="{{ url('/profile/photo') }}" enctype="multipart/form-data">
            @csrf
            <input id="profile_change_image" class="d-none" type="file" name="image">
            <button class="btn m-0-auto d-block" type="submit">{{ __('messages.save') }}</button>
        </form>
        @endif
        </div>
        <button type="submit" data-toggle="modal" data-target="#acount-cancel" class="btn btn-danger mt-5">{{__('messages.cancel_account')}}</button>
    </div>
    <div class="col-12 col-lg-8">
        <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                @component('profile.profile',['user' => $user])@endcomponent
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="acount-cancel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form action="{{ url('users', $user->id) }}" method="POST" class="modal-dialog" role="document">
        @csrf
        @method('DELETE')
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ __('messages.my_acount_delete_confirmation') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm " data-dismiss="modal">{{ __('messages.close') }}</button>
                <button type="submit" class="btn-danger btn btn-sm">{{ __('messages.cancel_acount') }}</button>
            </div>
        </div>
    </form>
</div>
