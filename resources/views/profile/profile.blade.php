<div class="profile__item">

    @include('components.show_messages')

    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#profile" role="tab" aria-controls="password" aria-selected="true">{{ __('messages.profile') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#cv" role="tab" aria-controls="cv" aria-selected="true">{{ __('messages.see_cv') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#password" role="tab" aria-controls="password" aria-selected="false">{{ __('messages.change_password') }}</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        {{-- Profile --}}
        <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="home-tab">

            <form method="post" action="{{ url('users', $user->id) }}" class="mt-3">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="f_first_name">{{ __('messages.name_user') }}</label>
                            <input type="text" class="form-control" id="f_first_name" name="first_name" value="{{ $user->first_name }}" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="f_last_name">{{ __('messages.last_name') }}</label>
                            <input type="text" class="form-control" id="f_last_name" name="last_name" value="{{ $user->last_name }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="f_email">{{ __('messages.email') }}</label>
                            <input type="email" class="form-control" id="f_email" value="{{ $user->email }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="f_birth_date">{{ __('messages.birthday') }}</label>
                            <input type="date" class="form-control" name="birth_date" id="f_birth_date" value="{{ $user->birth_date }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="f_phone">{{ __('messages.phone') }}</label>
                            <input type="text" class="form-control" name="phone" id="f_phone" value="{{ $user->phone }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="f_address">{{ __('messages.address') }}</label>
                            <input type="text" class="form-control" name="address" id="f_address" value="{{ $user->address }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="f_occupation">{{ __('messages.occupation') }}</label>
                            <input type="text" class="form-control" name="occupation" id="f_occupation" value="{{ $user->occupation }}">
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary">{{__('messages.save')}}</button>
            </form>

        </div>
        {{-- CV --}}
        <div class="tab-pane fade" id="cv" role="tabpanel" aria-labelledby="profile-tab">
            <div id="app" class="mt-3">
                <dropzone-component
                    title="{{ __('messages.dropzone_title') }}"
                    subtitle="{{ __('messages.dropzone_subtitle') }}"
                    removetext="{{ __('messages.dropzone_remove_title') }}"
                    url="{{ $user->cv }}"
                />
            </div>
        </div>
        {{-- Password --}}
        <div class="tab-pane fade" id="password" role="tabpanel" aria-labelledby="profile-tab">
            <form method="post" action="{{ url('users', $user->id) }}" class="mt-3">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="f_password">{{ __('messages.password') }}</label>
                            <input type="password" minlength="8" class="form-control" name="password" id="f_password" required>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">{{__('messages.change_password')}}</button>
            </form>
        </div>
    </div>
</div>
