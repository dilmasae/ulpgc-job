import Swiper from "swiper";

let swiper = new Swiper('.swiper-container', {
    autoplay: {
        delay: 5000,
    },
    slidesPerView: 3,
    spaceBetween: 30,
    slidesPerGroup: 3,
    loop: true,
    loopFillGroupWithBlank: true,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        1200: {
            slidesPerView: 4,
            spaceBetween: 25,
            slidesPerGroup: 1
        },
        768: {
            slidesPerView: 3,
            spaceBetween: 20,
            slidesPerGroup: 1
        },

        576: {
            slidesPerView: 2,
            spaceBetween: 20,
            slidesPerGroup: 1
        },

        200: {
            slidesPerView: 1,
            spaceBetween: 10,
            slidesPerGroup: 1
        }
    },
});
