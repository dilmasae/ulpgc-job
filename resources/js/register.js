import $ from "jquery";

const className = "d-none";

$("#register_rol").change(function() {
    if($(this).val() == "company") {
        $("#company_name-wrapper").removeClass(className);
        $("#company_name").attr("required", "true");
    }
    else {
        $("#company_name-wrapper").addClass(className);
        $("#company_name").attr("required", null);
    }
});
