import $ from "jquery";

$(".js-btn-confirm").on("click", function() {
    let identifier = $(this).data("identifier");
    let type = $(this).data("type");
    let modalWrapper = $("#btn-confirm-wrapper");
    let form = modalWrapper.find("form");

    if(type == "candidature") form.attr("action", `/jobs/candidature/${identifier}`);
    else if(type == "job") form.attr("action", `/jobs/${identifier}`);

    modalWrapper.modal('toggle');
});
