import $ from "jquery";

$(".profile-img").on('click', function() {
    $("#profile_change_image").click();
});

$("#profile_change_image").change(function(e) {
    readURL(this);
});

function readURL(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();

        reader.onload = function (e) {
            $('#profile_change_image_form img').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
